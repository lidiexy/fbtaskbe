<?php

namespace app\models;

use Yii;
use yii\base\Object;
use yii\helpers\Json;

/**
 * This is the model class Facebook Request public API.
 * Get all uploaded photos from user or page
 * eg: get http://graph.facebook.com/vanderbilt/photos/uploaded/?fields=id,name,source,created_time,images,name,likes.summary(true)&limit=50
 * Get one photo by id
 * eg: http://graph.facebook.com/10153333125436907/
 *
 * @property bool $https
 * @property string $page_id
 * @property integer $limit
 *
 * @author Lidiexy Alonso <lidiexy@gmail.com>
 * @link https://bitbucket.org/lidiexy/fbtaskbe/
 */

class FbRequest extends Object {
    const FB_HOST = 'graph.facebook.com';
    public $https = false;
    public $page_id = 'vanderbilt';
    public $limit = 1000;
    private $_fields = array(
        'name',
        'source',
        'likes.summary(true)'
    );

    public function getCustomPath() {
        $call_api = ($this->https) ? 'https://' : 'http://';
        $call_api .= self::FB_HOST . '/' . $this->page_id . '/photos/uploaded/';
        $call_params = '';
        $limit = '';
        if(sizeof($this->_fields) > 0) {
            $call_params = '?fields=' . implode(',',$this->_fields);
        }
        if($this->limit > 0) {
            $limit = '&limit=' . $this->limit;
        }
        return $call_api . $call_params . $limit;
    }

    public function getDefaultPath() {
        $call_api = ($this->https) ? 'https://' : 'http://';
        $call_api .= self::FB_HOST . '/vanderbilt/photos/uploaded/';
        $call_params = '';
        $limit = '';
        if(sizeof($this->_fields) > 0) {
            $call_params = '?fields=' . implode(',',$this->_fields);
        }
        if($this->limit > 0) {
            $limit = '&limit=' . $this->limit;
        }
        return $call_api . $call_params . $limit;
    }

    public function getIdPath() {
        $call_api = ($this->https) ? 'https://' : 'http://';
        $call_api .= self::FB_HOST . '/vanderbilt/photos/uploaded/';
        $call_params = '?fields=id&limit=1000';
        return $call_api . $call_params;
    }

    public function getPhotoByIdPath($id) {
        $call_api = ($this->https) ? 'https://' : 'http://';
        $call_api .= self::FB_HOST . '/' . $id . '/';
        $call_params = '';
        if(sizeof($this->_fields) > 0) {
            $call_params = '?fields=' . implode(',',$this->_fields);
        }
        return $call_api . $call_params;
    }

    public function setFields($new_fields = []){
        if(sizeof($new_fields) > 0) {
            foreach ($new_fields as $param) {
                $this->_fields[] = $this->slug($param);
            }
        }
    }

    public function getFields() {
        return $this->_fields;
    }

    private function slug($param){
        $param = strtolower($param);
        $param = preg_replace('/[^a-z0-9 -]+/', '', $param);
        $param = str_replace(' ', '-', $param);
        return urlencode(trim($param, '-'));
    }

    public function &facebookIdToArray(&$curl, $url) {
        static $array_fb_ids = [];
        $result = $curl->get($url);
        if($curl->responseCode == 200) {
            //success logic here
            $data = Json::decode($result);
            foreach ($data['data'] as $value) {
                $array_fb_ids[$value['id']]['name'] = (isset($value['name']) && !empty($value['name'])) ? $value['name'] : 'Vanderbilt University';
                $array_fb_ids[$value['id']]['created_time'] = $value['created_time'];
                $array_fb_ids[$value['id']]['source'] = $value['source'];
                $array_fb_ids[$value['id']]['likes'] = $value['likes']['summary']['total_count'];
            }
            if(isset($data['paging']['next']) && !empty($data['paging']['next'])) {
                $array_fb_ids = $this->facebookIdToArray($curl, $data['paging']['next']);
            }
            return $array_fb_ids;
        } else {
            return [];
        }
    }
}