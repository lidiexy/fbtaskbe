<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Vanderbilt Facebook Gallery Admin';
?>
<div class="site-index">

    <div class="jumbotron">
        <img src="images/vanderbilt-logo.jpg" class="img-circle" alt="Vanderbilt University" title="Vanderbilt University"/>
        <h1>Facebook Task!</h1>
        <p class="lead">Back-End App to manage the Facebook Photo Gallery of Vanderbilt University.</p>

        <p>
        <?= Html::a('Go to the gallery', ['gallery/index'], [
            'class' => 'btn btn-lg btn-success'
        ]) ?>
        </p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><span class="glyphicon glyphicon-picture"></span> <?= $count_images; ?> photos </h2>
                <p> More photos and comments in Vanderbilt Facebook Page <a href="https://www.facebook.com/vanderbilt">https://www.facebook.com/vanderbilt</a></p>

                <div class="well"> Vanderbilt is a dynamic center of research, learning and growth - a private research university of 6,300 undergrads and 5,300 grad/professional students.</div>
            </div>
            <div class="col-lg-4">
                <h2>Last uploaded</h2>
                <?php
                $date = DateTime::createFromFormat('Y-m-d H:i:s', $last_uploaded->created_time);
                ?>
                <p>The image above was uploaded on <?= $date->format("l jS \of F Y h:i:s A") ?></p>
                <img src="<?= $last_uploaded->source; ?>" class="img-responsive" alt="<?= $last_uploaded->name; ?>" title="<?= $last_uploaded->name; ?>"/>
                <p><?= $last_uploaded->name; ?></p>
            </div>
            <div class="col-lg-4">
                <h2>Top likes: <?= $top_likes->likes; ?></h2>
                <p>Many people like this photo!</p>
                <img src="<?= $top_likes->source; ?>" class="img-responsive" alt="<?= $top_likes->name; ?>" title="<?= $top_likes->name; ?>"/>
                <p><?= $top_likes->name; ?></p>
            </div>
        </div>

    </div>
</div>
