<?php
use yii\helpers\Url;

$this->title = 'Processing... the Vanderbilt Photo Gallery';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="jumbotron">
        <img src="images/vanderbilt-logo.jpg" class="img-circle" alt="Vanderbilt University" title="Vanderbilt University"/>
        <h1>Processing...!!!</h1>
        <p class="help-block">Please, wait until the process ends. You will be redirected after that.</p>
        <div id="spinner">
            <img src="images/loading_nobg.gif" alt="loading..." title="loading..."/>
        </div>
    </div>
</div>

<script>
window.onload = function(){
    $.ajax({
        type: 'get',
        dataType: 'json',
        //async: false,
        url: '<?= Url::to(['loader/loader']); ?>'
    }).success(function(data){
        if(data.success) {
            $('#spinner').html('<div class="alert alert-success">Process complete, Congratulation!!.</div>');
            setTimeout(function() {
                window.location.href = '<?= Url::to(['index']); ?>';
            }, 1000);
        } else {
            $('#spinner').html('<div class="alert alert-danger">ERROR in the process!! Sorry.</div>');
            setTimeout(function() {
                window.location.href = '<?= Url::to(['index']); ?>';
            }, 2000);
        }
    }).error(function(){
        $('#spinner').html('<div class="alert alert-danger">ERROR in the process!! Sorry.</div>');
        setTimeout(function() {
            window.location.href = '<?= Url::to(['index']); ?>';
        }, 1000);
    });
};
</script>
