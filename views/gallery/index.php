<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchGallery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Facebook - Vanderbilt Photo Gallery';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="form-group">
        <?php
        if($dataProvider->count > 0) {
            echo Html::a('Search for more images', ['searchmore'], ['class' => 'btn btn-success']);
        } else {
            echo Html::a('Start the data process', ['searchmore'], ['class' => 'btn btn-success']);
        }
        ?>
        <?= Html::a('Create Image (Not Recomended)', ['create'], ['class' => 'btn btn-warning']); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'created_time:datetime',
            'likes',
            [
                'attribute' => 'source',
                'format'=>'raw',
                'value'=>function($data) {
                    return Html::a('URL to image',$data->source,['target'=>'_blank']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
