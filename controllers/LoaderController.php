<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use linslin\yii2\curl;
use app\models\FbRequest;
use app\models\Gallery;


class LoaderController extends Controller  {

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Insert all the new images in the Vanderbilt University FaceBook Page
     *
     * @param array $values_to_insert
     * @param array $array_from
     * @return bool
     * @throws \yii\db\Exception
     */
    private function insertNewPhotos($values_to_insert = [], $array_from = []){
        $data = [];
        $array_result = [];
        $doit = null;
        if(count($values_to_insert) > 0 && count($array_from) > 0) {
            foreach($values_to_insert as $obj){
                $data['id'] = $obj;
                $data['name'] = $array_from[$obj]['name'];
                $data['created_time'] = $array_from[$obj]['created_time'];
                $data['source'] = $array_from[$obj]['source'];
                $data['likes'] = $array_from[$obj]['likes'];
                $array_result[] = $data;
            }
            $doit = Yii::$app->db->createCommand()->batchInsert(Gallery::tableName(),array_keys($data), $array_result)->execute();
        }
        if(isset($doit) && !empty($doit)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove all the images that had been erased from Vanderbilt University FaceBook Page
     *
     * @param array $values_to_remove
     * @return bool
     */
    private function removeErasedPhotos($values_to_remove = []) {
        if(sizeof($values_to_remove) > 0) {
            foreach($values_to_remove as $id) {
                $model = Gallery::findOne($id);
                if(isset($model)) {
                    $model->delete(false);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Loader Action is the trigger to wrap all images from Vanderbilt University FaceBook Page
     * If this function would be part of one CRON task, the first conditional must be removed.
     * @return string jSON Result
     */
    public function actionLoader() {
        if (Yii::$app->request->isAjax) { //Erase this condition if need run this function directly
            //Init curl
            $curl = new curl\Curl();
            //Init Facebook Graph API request to obtain the default URL
            $request = new FbRequest();
            //Get the full array from the Grpah API, the url configured it's passed as a param.
            $array_fb_full = $request->facebookIdToArray($curl, $request->getDefaultPath());
            //Get the array of keys (ids) to calculate A diff B (rows to be added)
            $array_fb_ids = array_keys($array_fb_full);
            //Get the array of keys (ids) to calculate B diff A (rows to be removed)
            $array_model_ids = ArrayHelper::getColumn(
                Gallery::find()->select('id')->asArray()->all(),
                'id'
            );
            //A diff B (rows to be added)
            $values_to_insert = array_diff($array_fb_ids, $array_model_ids);
            //B diff A (rows to be removed)
            $values_to_remove = array_diff($array_model_ids, $array_fb_ids);
            //Process the values to insert and remove from Model Gallery
            if (sizeof($values_to_insert) > 0) {
                $this->insertNewPhotos($values_to_insert, $array_fb_full);
            }

            if (sizeof($values_to_remove) > 0) {
                $this->removeErasedPhotos($values_to_remove);
            }

            return Json::encode(['success' => true, 'message' => 'Process have been done']);
        }
    }

}
